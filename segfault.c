#include <signal.h>

#include <registryFunction.h>
#include <epicsExport.h>

static void segfault_registrar(void) {
    raise(SIGSEGV);
}

epicsExportRegistrar(segfault_registrar);
